import cv2
import cv2.aruco as aruco
import numpy as np
import math 
#  use 6, 7, 2 id arugo markers
'''
2
|
|
6 ____ 7
'''
cap = cv2.VideoCapture(0)
ret, frame = cap.read()
averageValue1 = np.float32(frame)

while(True):
    ret, frame = cap.read() 
    frame1 = frame[65:380,55:645]
    # using the cv2.accumulateWeighted() function 
    # that updates the running average 
    cv2.accumulateWeighted(frame, averageValue1, 0.1)
    # converting the matrix elements to absolute values  
    # and converting the result to 8-bit.  
    frame = cv2.convertScaleAbs(averageValue1) 
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)
    parameters =  aruco.DetectorParameters_create()
    corners, ids, rejectedImgPoints = aruco.detectMarkers(gray, aruco_dict, parameters=parameters)
    final = 0
    final2 = 0
    final3 = 0
    flag = 0
    font=cv2.FONT_HERSHEY_COMPLEX
    blank_image = np.zeros(shape=[768, 1366, 3], dtype=np.uint8)

    if(corners != [] and len(ids)>=3):
        # gray= cv2.line(gray, (corners[0][0][0][0], corners[0][0][0][1]), (corners[0][0][1][0], corners[0][0][1][1]), (255, 0, 0), 10)
        # gray= cv2.line(gray, (corners[0][0][1][0], corners[0][0][1][1]), (corners[0][0][2][0], corners[0][0][2][1]), (255, 0, 0), 10)
        # gray= cv2.line(gray, (corners[0][0][2][0], corners[0][0][2][1]), (corners[0][0][3][0], corners[0][0][3][1]), (255, 0, 0), 10)
        # gray= cv2.line(gray, (corners[0][0][3][0], corners[0][0][3][1]), (corners[0][0][0][0], corners[0][0][0][1]), (255, 0, 0), 10)
        # gray= cv2.line(gray, (corners[1][0][0][0], corners[1][0][0][1]), (corners[1][0][1][0], corners[1][0][1][1]), (0, 0, 0), 10)
        # gray= cv2.line(gray, (corners[1][0][1][0], corners[1][0][1][1]), (corners[1][0][2][0], corners[1][0][2][1]), (0, 0, 0), 10)
        # gray= cv2.line(gray, (corners[1][0][2][0], corners[1][0][2][1]), (corners[1][0][3][0], corners[1][0][3][1]), (0, 0, 0), 10)
        # gray= cv2.line(gray, (corners[1][0][3][0], corners[1][0][3][1]), (corners[1][0][0][0], corners[1][0][0][1]), (0, 0, 0), 10)
        
        # top = corners[0][0]
        # bot = corners[1][0]

        for i in range(len(ids)):
            if(ids[i]==6):
                a = i
            if(ids[i]==7):
                b = i
            if(ids[i]==2):
                c = i   
        
        c0 = corners[a][0]
        c1 = corners[b][0]
        x0 = [c0[:, 0].mean(), c0[:, 1].mean()]
        x1 = [c1[:, 0].mean(), c1[:, 1].mean()]
        distbw = math.sqrt(math.pow(x0[0]-x1[0],2)+math.pow(x0[1]-x1[1],2))
        dist4 = math.sqrt(math.pow(corners[0][0][0][0]-corners[0][0][1][0], 2) + math.pow(corners[0][0][0][1]-corners[0][0][1][1], 2))

        c2 = corners[c][0]
        x2 = [c2[:, 0].mean(), c2[:, 1].mean()]
        distbw2 = math.sqrt(math.pow(x0[0]-x2[0],2)+math.pow(x0[1]-x2[1],2))

        final = 4.0*distbw/dist4
        s="Side 1: "
        s+=str(round(final,3))
        final2 = 4.0*distbw2/dist4
        final2 = 4.0*distbw2/dist4
        #gray = aruco.drawDetectedMarkers(gray, corners)
        if flag == 0:
            cv2.putText(blank_image, s, (600, 30), font, 1, (0, 0, 255), 1, cv2.FILLED)
            s="Side 2: "
            s+=str(round(final2, 3))
            cv2.putText(blank_image, s, (600, 60), font, 1, (0,255,0), 1, cv2.FILLED)
        else:
            cv2.putText(blank_image, "Please make the right angle traingle", (600, 30), font, 1, (0, 0, 255), 1, cv2.FILLED)

        distbw3 = math.sqrt(math.pow(x1[0]-x2[0],2)+math.pow(x1[1]-x2[1],2))
        final3 = 4.0*distbw3/dist4
    

        x = []
        y = []
        for i in range(len(ids)):
            cor = corners[i][0]
            # plt.plot([c[:, 0].mean()], [c[:, 1].mean()], "o", label = "id={0}".format(ids[i]))
            x.append(int(cor[:, 0].mean()*2))
            y.append(int(cor[:, 1].mean()*2))

        #    plt.plot([x[0], x[1], x[2], x[0]],[y[0], y[1], y[2], y[0]])
        frame_markers = cv2.line(blank_image, (x[a], y[a]), (x[c],y[c]), (0,255,0), 3)
        frame_markers = cv2.line(blank_image, (x[a], y[a]), (x[b],y[b]), (0, 0, 255), 3)
        frame_markers = cv2.line(blank_image, (x[b], y[b]), (x[c],y[c]), (255, 0, 0), 3)
        # frame_markers = cv2.line(frame, (x[1], y[1]), (x[2],y[2]), (0,255,0), 3)

        if(abs(x[a] - x[c]) > 80 or abs(y[a] - y[b]) > 80) :
            flag = 1
        else:
            flag = 0

    c_root = round(math.sqrt(final*final + final2*final2),2)
    a = round(final)
    b = round(final2)
    c = a*a + b*b
    sup = "                                       2     2      2"
    ans = "We know, by pathagoras theoram, a  + b    = c "
    cv2.putText(blank_image, sup, (8, 60), font, 0.5, (0,255,0), 1, cv2.FILLED)
    cv2.putText(blank_image, ans, (8, 70), font, 0.5, (0,255,0), 1, cv2.FILLED)
    # print(blank_image.shape)
    ans = "here, a = " + str(a) + " and b = " + str(b)
    cv2.putText(blank_image, ans, (8, 100), font, 0.5, (0,255,0), 1, cv2.FILLED)
    sup = "              2               2"
    cv2.putText(blank_image, sup, (8, 130), font, 0.5, (255,255,255), 1, cv2.FILLED)
    ans = "Therefore, a   = " + str(a*a) + " and  b   = " + str(b*b)
    cv2.putText(blank_image, ans, (8, 140), font, 0.5, (255,255,255), 1, cv2.FILLED)
    ans = "Using the above formula,"
    cv2.putText(blank_image, ans, (8, 170), font, 0.5, (0,255,0), 1, cv2.FILLED)
    sup = "  2"
    cv2.putText(blank_image, sup, (8, 200), font, 0.5, (255,255,255), 1, cv2.FILLED)
    ans = "c   = " +  str(a*a) + " + " + str(b*b) + "  = " + str(c) 
    cv2.putText(blank_image, ans, (8, 210), font, 0.5, (255,255,255), 1, cv2.FILLED)
    sup = "               _____"
    cv2.putText(blank_image, sup, (8, 240), font, 0.5, (255,255,255), 1, cv2.FILLED)
    ans = "Hence, c = _/" + str(c) + "   = " + str(c_root) + " (In theory)" 
    cv2.putText(blank_image, ans, (8, 255), font, 0.5, (255,255,255), 1, cv2.FILLED)

    # now lets find the practical value:
    ans = "Actual measurement = " + str(round(final3, 2)) 
    cv2.putText(blank_image, ans, (8, 280), font, 0.5, (0,255,0), 1, cv2.FILLED)


    # Lets solve quadratic equations !
    var = 6
    ans = "________________________________________________________"
    
    cv2.putText(blank_image, ans, (8, 300), font, 0.5, (0,0,255), 1, cv2.FILLED)
    ans = "Lets assume: a = x"  + " and b = x + " + str(var)
    cv2.putText(blank_image, ans, (8, 330), font, 0.5, (255,255,255), 1, cv2.FILLED)

    sup = "       2     2    2"
    cv2.putText(blank_image, sup, (8, 360), font, 0.5, (255,255,255), 1, cv2.FILLED)
    ans = "Now, a  + b  = c "
    cv2.putText(blank_image, ans, (8, 370), font, 0.5, (255,255,255), 1, cv2.FILLED)
    
    sup = "      2           2    2"
    cv2.putText(blank_image, sup, (8, 400), font, 0.5, (255,255,255), 1, cv2.FILLED)
    ans = "=> x  + (x + " + str(var) + ")  = c "
    cv2.putText(blank_image, ans, (8, 410), font, 0.5, (255,255,255), 1, cv2.FILLED)
    
    sup = "      2    2                  2"
    cv2.putText(blank_image, sup, (8, 440), font, 0.5, (255,255,255), 1, cv2.FILLED)
    ans = "=> x  + x  + " + str(var*2) +"x + " + str(var*var) +" = c"
    cv2.putText(blank_image, ans, (8, 450), font, 0.5, (255,255,255), 1, cv2.FILLED)
    
    sup = "       2                  2"
    cv2.putText(blank_image, sup, (8, 480), font, 0.5, (255,255,255), 1, cv2.FILLED)
    ans = "=> 2x  + " + str(var*2) + "x + " + str(var*var) +" - c  = 0"
    cv2.putText(blank_image, ans, (8, 490), font, 0.5, (255,255,255), 1, cv2.FILLED)
 
    sup = "       2                      "
    cv2.putText(blank_image, sup, (8, 510), font, 0.5, (255,255,255), 1, cv2.FILLED)
    c = round(c,2)
    ans = "=> 2x  + 12x + 36 -" + str(c) + " = 0"
    cv2.putText(blank_image, ans, (8, 520), font, 0.5, (255,255,255), 1, cv2.FILLED)
    
    ans = " - - - - - - - - - - - - - - - - - - - - - - - -"
    cv2.putText(blank_image, ans, (8, 540), font, 0.5, (255,0,0), 1, cv2.FILLED)
    
    sup = "                                               _____________"
    cv2.putText(blank_image, sup, (8, 560), font, 0.5, (0,255,0), 1, cv2.FILLED)
    ans = "Remember the formula : X = -B (+|-) __/(B*B - 4AC)"
    cv2.putText(blank_image, ans, (8, 575), font, 0.5, (0,255,0), 1, cv2.FILLED)
    sup = "                                  _________________________"
    cv2.putText(blank_image, sup, (8, 580), font, 0.5, (0,255,0), 1, cv2.FILLED)
    ans = "                                             2A"
    cv2.putText(blank_image, ans, (8, 600), font, 0.5, (0,255,0), 1, cv2.FILLED)
    
    c = var*var - c
    ans = "Here, A = 1, B = 12, C = " + str(c)
    cv2.putText(blank_image, ans, (600, 130), font, 0.5, (255,255,255), 1, cv2.FILLED)
    sup = "                        __________________________"
    cv2.putText(blank_image, sup, (600, 155), font, 0.5, (255,255,255), 1, cv2.FILLED)
    ans = "=> X = " + str(-var*2) + " (+|-) __/ 12x12 - 4x1x (" + str(abs(c)) + ")"
    cv2.putText(blank_image, ans, (600, 170), font, 0.5, (255,255,255), 1, cv2.FILLED)
    sup = "         ________________________________________"
    cv2.putText(blank_image, sup, (600, 180), font, 0.5, (255,255,255), 1, cv2.FILLED)
    ans = "                           2x1"
    cv2.putText(blank_image, ans, (600, 200), font, 0.5, (255,255,255), 1, cv2.FILLED)

    determinant = (var*2)*(var*2) - 4*c
    # print(determinant)
    # print(math.sqrt(determinant))
    
    sup = "                        _________"
    cv2.putText(blank_image, sup, (600, 225), font, 0.5, (255,255,255), 1, cv2.FILLED)
    ans = "=> X = " + str(-var*2) + " (+|-) __/ " + str(round(math.sqrt(determinant),2))
    cv2.putText(blank_image, ans, (600, 240), font, 0.5, (255,255,255), 1, cv2.FILLED)
    sup = "         _______________________"
    fin = -(var*2) + math.sqrt(determinant)
    state = str(round(fin, 2))
    neg = -(var*2) - math.sqrt(determinant)
    sup+= "  => X = " + str(round(fin)) + " or " + str(round(neg, 2))
    cv2.putText(blank_image, sup, (600, 245), font, 0.5, (255,255,255), 1, cv2.FILLED)
    ans = "                       2x1"
    cv2.putText(blank_image, ans, (600, 260), font, 0.5, (255,255,255), 1, cv2.FILLED)

    if(fin<=0):
        fin = 0
    ans = "Because length can't be less than 0. Therefore X = " + str(round(fin, 2))
    cv2.putText(blank_image, ans, (600, 290), font, 0.5, (255,255,255), 1, cv2.FILLED)

    cv2.imshow("Solution", blank_image)
    cv2.imshow('frame', frame1)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()